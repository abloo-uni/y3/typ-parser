const fs = require("fs");

console.time('test');
var myArgs = process.argv.slice(2);
var myFlags = myArgs.filter(a => a.startsWith('-')).map(b => myArgs.indexOf(b)); //get indexes of flags in array

//Determine file name
var filename = myArgs[0].substring(0, myArgs[0].length-4)+"smv";

if (myArgs.length == 0){ //check if no arguments are specified
	console.log("Error: No arguments inputted, see -h or --help for usage"); 
	process.exit(1);
}

//flag checker
myFlags.forEach( f => {
	if(myArgs[f]=='-h' || myArgs[f]=='--help'){
		console.log("Usage: node parser.js [OPTION]... [JSON FILE]...\nParse a json policy to model checking NuSMV Form.\n\n\t-o, --output [JSON FILE]\tthe output file, [FILE] for the parser, must be in JSON format\n\t-h, --help\t\t\tdisplay this help\n");
		process.exit(0);
	}
	if(myArgs[f].startsWith('-o') || myArgs[f].startsWith('--output')){
		if(myArgs[f] == '-o' || myArgs == '--output'){
			filename = myArgs[f+1];
		}
		else if (myArgs[f].startsWith('-o')){
			filename = myArgs[f].substring(2);
		}
		else{
			filename = myArgs[f].substring(8);
		}
	}
	//if no file name specified with -o flag, the filename will just be the input filename but with a .smv suffix
})

try{
	fs.readFileSync(myArgs[myArgs.length-1], 'utf8');
} catch(err){
	console.log("Error: no such file");
	process.exit(1);
}
const inFile = fs.readFileSync(myArgs[myArgs.length-1], 'utf8');

try{
	JSON.parse(inFile);
} catch(err){
	console.log("Error: Inputted file does not have valid JSON format");
	process.exit(1);
}
try{
	const inObj = JSON.parse(inFile);
	var outFile = `-- test smv file generation\n\n`;

	// the file is assumed to have the user assignments to roles in a "bindings" section, see TYP Report for info
	if(inObj.bindings){ //check if bindings section exists
		var memberlist = [... new Set(inObj.bindings.map(b => b.members).flat().map(a=> `${a}`))]; //get an array of the unique members in each object in bindings
		outFile+=(`MODULE main\n\nVAR\n\tMEMBER : {ANY, ${memberlist.map(m => `"${m}"`)}};\n`); // add formatted list to output string
		var rolelist = inObj.bindings.map(b => `"${b.role}"`); //get an array of all roles in bindings
		outFile+=(`\tROLE : {ANY, ${rolelist}};\n`); // add formatted array to output string
	}

	if(inObj.permissions){
		var permslist = [... new Set(inObj.permissions.map(b => b.includedPermissions).flat().map(a=> `"${a}"`))]; //get an array of all unique permissions found in permissions section
		outFile+=(`\tPRMS : {ANY, ${permslist}};\n`); // add formatted array to output string
	}

	if(inObj.serviceaccounts){
		var resourcelist = [... new Set(inObj.serviceaccounts.map(b => b.projectId).flat().map(a=> `"${a}"`))]; //get array of all the unique resources found in the serviceaccounts
		outFile+=(`\tRSRC : {ANY, ${resourcelist}};\n`); // add formatted array to output string
	}

	//add essential lines to output string
	outFile+=(`\tRH : {ANY, Resources, Projects, Organization};\n\n\tRBACGCP : RBACGCP (MEMBER, ROLE, PRMS, RSRC, RH);\n`); 
	outFile+=(`\nASSIGN\n\tnext (MEMBER) := MEMBER ;\n\tnext (ROLE) := ROLE ;\n\tnext (PRMS) := PRMS ;\n\tnext (RSRC) := RSRC ;\n\tnext (RH) := RH ;\n`);
	outFile+=(`\n\nMODULE RBACGCP (MEMBER, ROLE, PRMS, RSRC, RH)\nVAR\n\tdecision : {Grant, Deny};\n\nASSIGN\n\tinit (decision) := Deny;\n\tnext (decision) := case\n\n	-- Definition of policies\n`);

	//(MEMBER = | MEMBER = ANY) & (ROLE = | ROLE = ANY) & (PRMS = | PRMS = | PRMS = ANY) & (RSRC = "project_a" | RSRC = ANY) & (RH = Projects | RH = ANY) : Grant ;

	// generate policys per member per resource
	memberlist.forEach( m => { //iterate through members and resources
		resourcelist.forEach( re => {
			let rolespermember = inObj.bindings.filter(b => b.members.includes(m)).map(a => a.role); //get an array of all roles for the member
			let roleoutput = rolespermember.map(a => `ROLE = "${a}" |`).join(' '); //use the array of roles to write to the string
			var permspermember = [];
			rolespermember.forEach(rpm => { 
				permspermember = permspermember.concat(inObj.permissions.filter(p => p.name == rpm).map(f => f.includedPermissions).flat()); //iterate through the roles per member to build an array of all permissions
			});
			permoutput = [... new Set(permspermember)].map(f => `PRMS = "${f}" |`).join (' '); //change array to contain only unique permissions and format it to string to be outputted
			// long output string to declare the policys using the above defined arrays & strings
			outFile+=(`\n\t(MEMBER = "${m}" | MEMBER = ANY) & (${roleoutput} ROLE = ANY) & (${permoutput} PRMS = ANY) & (RSRC = ${re} | RSRC = ANY) & (RH = Resources | RH = ANY) : Grant ;\n`);
		});
	});

	outFile+=(`\n\t-- Default decision\n\tTRUE : Deny;\nesac;`);

	/*
	SPEC AG ((MEMBER = "alice@gmail.com") & (ROLE = "roles/pubsub.publisher") & (PRMS = ANY) & (RSRC = "project_a") & (RH = Projects) -> AF decision = Grant) 

	-- Specification 2. Evaluates to FALSE.
	SPEC AG ((MEMBER = "alice@gmail.com") & (ROLE = ANY) & (PRMS = "pubsub.topics.publish") & (RSRC = "project_a") & (RH = Projects) -> AF decision = Grant) 

	-- Specification 3. Evaluates to FALSE.
	SPEC AG ((MEMBER = "alice@gmail.com") & (ROLE = ANY) & (PRMS = "pubsub.topics.delete") & (RSRC = "topic_a") & (RH = Resources) -> AF decision = Grant) 
	*/

	fs.writeFileSync(filename, outFile); //write output string to file with the earlier  determined filename

}catch(err){
	console.log(err);
}
console.log(`File: ${filename} generated`);
console.timeEnd('test');